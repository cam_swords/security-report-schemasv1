#!/bin/bash

PROJECT_DIRECTORY=$(realpath "$(dirname "$(realpath "${BASH_SOURCE[0]}")")/..")
DS_SCHEMA="$PROJECT_DIRECTORY/dist/dependency-scanning-report-format.json"

source "$PROJECT_DIRECTORY/test/helper_functions.sh"
source "$PROJECT_DIRECTORY/test/common-tests.sh"

setup_suite() {
  regenerate_dist_schemas
}

test_dependency_scanning_contains_common_definitions() {
  ensure_common_definitions "$DS_SCHEMA" '["dependency_scanning"]'
}

test_dependency_scanning_extensions() {
  verify_schema_contains_selector "$DS_SCHEMA" 'select(.properties.vulnerabilities.items.required[] | contains("location"))'
  verify_schema_contains_selector "$DS_SCHEMA" ".properties.vulnerabilities.items.properties.location.properties.file"
  verify_schema_contains_selector "$DS_SCHEMA" ".properties.vulnerabilities.items.properties.location.properties.dependency"

  verify_schema_contains_selector "$DS_SCHEMA" 'select(.required[] | contains("dependency_files"))'
  verify_schema_contains_selector "$DS_SCHEMA" ".properties.dependency_files.items.properties.path"
  verify_schema_contains_selector "$DS_SCHEMA" ".properties.dependency_files.items.properties.package_manager"
  verify_schema_contains_selector "$DS_SCHEMA" ".properties.dependency_files.items.properties.dependencies.items.properties.package.properties.name"
  verify_schema_contains_selector "$DS_SCHEMA" ".properties.dependency_files.items.properties.dependencies.items.properties.version"
  verify_schema_contains_selector "$DS_SCHEMA" ".properties.dependency_files.items.properties.dependencies.items.properties.iid"
  verify_schema_contains_selector "$DS_SCHEMA" ".properties.dependency_files.items.properties.dependencies.items.properties.direct"
  verify_schema_contains_selector "$DS_SCHEMA" ".properties.dependency_files.items.properties.dependencies.items.properties.dependency_path"
}
