import {merge} from '../../support/merge'

const defaults = {
  scanner: {
    id: 'bundler_audit',
    name: 'bundler-audit',
    url: 'https://github.com/rubysec/bundler-audit',
    vendor: {
      name: 'GitLab'
    },
    version: '0.7.0.1',
  },
  start_time: '2020-12-04T03:26:07',
  end_time: '2020-12-04T03:26:19',
  type: 'dependency_scanning',
  status: 'success'
}

export const scan = (scanOverrides) => {
  return merge(defaults, scanOverrides)
}
