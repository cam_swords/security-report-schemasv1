import b from './builders/index'
import {schemas} from './support/schemas'

describe('secret detection schema', () => {

  it('should validate location', () => {
    const report = b.secret_detection.report({
      vulnerabilities: [b.secret_detection.vulnerability({
        location: {
          file: 'Main.java',
          class: 'Main',
          method: 'main',
          start_line: 10,
          end_line: 20,
          commit: {
            author: 'Fred FlintStone',
            date: '2020-01-01T00:00:00Z',
            sha: '7fa11597121a966f7b2c5ac5bc91b70e0c8d0558',
            message: 'Yabba Dabba Doo'
          }
        }
      })]
    })

    expect(schemas.secret_detection.validate(report).success).toBeTruthy()
  })

  it('location commit is required', () => {
    const report = b.secret_detection.report({
      vulnerabilities: [b.secret_detection.vulnerability({
        location: {commit: undefined}
      })]
    })

    expect(schemas.secret_detection.validate(report).errors).toContain('location requires property "commit"')
  })

  it('commit sha is required', () => {
    const report = b.secret_detection.report({
      vulnerabilities: [b.secret_detection.vulnerability({
        location: {
          commit: {
            author: 'Fred FlintStone',
            date: '2020-01-01T00:00:00Z',
            message: 'Yabba Dabba Doo'
          }
        }
      })]
    })

    expect(schemas.secret_detection.validate(report).errors).toContain('commit requires property "sha"')
  })
})
