#!/bin/bash

PROJECT_DIRECTORY=$(realpath "$(dirname "$(realpath "${BASH_SOURCE[0]}")")/..")
DAST_SCHEMA="$PROJECT_DIRECTORY/dist/dast-report-format.json"

source "$PROJECT_DIRECTORY/test/helper_functions.sh"
source "$PROJECT_DIRECTORY/test/common-tests.sh"

setup_suite() {
  regenerate_dist_schemas
}

test_dast_contains_common_definitions() {
  ensure_common_definitions "$DAST_SCHEMA" '["dast", "api_fuzzing"]'
}

test_dast_extensions() {
  verify_schema_contains_selector "$DAST_SCHEMA" 'select(.properties.vulnerabilities.items.required[] | contains("location"))'

  verify_schema_contains_selector "$DAST_SCHEMA" ".properties.vulnerabilities.items.properties.discovered_at"
  verify_schema_contains_selector "$DAST_SCHEMA" ".properties.vulnerabilities.items.properties.location.properties.hostname"
  verify_schema_contains_selector "$DAST_SCHEMA" ".properties.vulnerabilities.items.properties.location.properties.param"
  verify_schema_contains_selector "$DAST_SCHEMA" ".properties.vulnerabilities.items.properties.location.properties.method"
  verify_schema_contains_selector "$DAST_SCHEMA" ".properties.vulnerabilities.items.properties.location.properties.path"
  verify_schema_contains_selector "$DAST_SCHEMA" ".properties.vulnerabilities.items.properties.evidence.properties.source.properties.id"
  verify_schema_contains_selector "$DAST_SCHEMA" ".properties.vulnerabilities.items.properties.evidence.properties.source.properties.name"
  verify_schema_contains_selector "$DAST_SCHEMA" ".properties.vulnerabilities.items.properties.evidence.properties.source.properties.url"
  verify_schema_contains_selector "$DAST_SCHEMA" ".properties.vulnerabilities.items.properties.evidence.properties.summary"
  verify_schema_contains_selector "$DAST_SCHEMA" ".properties.vulnerabilities.items.properties.evidence.properties.request.properties.headers"
  verify_schema_contains_selector "$DAST_SCHEMA" ".properties.vulnerabilities.items.properties.evidence.properties.request.properties.method"
  verify_schema_contains_selector "$DAST_SCHEMA" ".properties.vulnerabilities.items.properties.evidence.properties.request.properties.url"
  verify_schema_contains_selector "$DAST_SCHEMA" ".properties.vulnerabilities.items.properties.evidence.properties.request.properties.body"
  verify_schema_contains_selector "$DAST_SCHEMA" ".properties.vulnerabilities.items.properties.evidence.properties.response.properties.headers"
  verify_schema_contains_selector "$DAST_SCHEMA" ".properties.vulnerabilities.items.properties.evidence.properties.response.properties.reason_phrase"
  verify_schema_contains_selector "$DAST_SCHEMA" ".properties.vulnerabilities.items.properties.evidence.properties.response.properties.status_code"
  verify_schema_contains_selector "$DAST_SCHEMA" ".properties.vulnerabilities.items.properties.evidence.properties.response.properties.body"

  verify_schema_contains_selector "$DAST_SCHEMA" ".properties.scan.properties.scanned_resources.items.properties.method"
  verify_schema_contains_selector "$DAST_SCHEMA" ".properties.scan.properties.scanned_resources.items.properties.url"
  verify_schema_contains_selector "$DAST_SCHEMA" ".properties.scan.properties.scanned_resources.items.properties.type"

  verify_schema_contains_selector "$DAST_SCHEMA" ".properties.vulnerabilities.items.properties.evidence.properties.supporting_messages.items.properties.name"
  verify_schema_contains_selector "$DAST_SCHEMA" ".properties.vulnerabilities.items.properties.evidence.properties.supporting_messages.items.properties.request.properties.headers"
  verify_schema_contains_selector "$DAST_SCHEMA" ".properties.vulnerabilities.items.properties.evidence.properties.supporting_messages.items.properties.request.properties.method"
  verify_schema_contains_selector "$DAST_SCHEMA" ".properties.vulnerabilities.items.properties.evidence.properties.supporting_messages.items.properties.request.properties.url"
  verify_schema_contains_selector "$DAST_SCHEMA" ".properties.vulnerabilities.items.properties.evidence.properties.supporting_messages.items.properties.request.properties.body"
  verify_schema_contains_selector "$DAST_SCHEMA" ".properties.vulnerabilities.items.properties.evidence.properties.supporting_messages.items.properties.response.properties.headers"
  verify_schema_contains_selector "$DAST_SCHEMA" ".properties.vulnerabilities.items.properties.evidence.properties.supporting_messages.items.properties.response.properties.reason_phrase"
  verify_schema_contains_selector "$DAST_SCHEMA" ".properties.vulnerabilities.items.properties.evidence.properties.supporting_messages.items.properties.response.properties.status_code"
  verify_schema_contains_selector "$DAST_SCHEMA" ".properties.vulnerabilities.items.properties.evidence.properties.supporting_messages.items.properties.response.properties.body"

  verify_schema_contains_selector "$DAST_SCHEMA" ".properties.vulnerabilities.items.properties.assets.items.properties.type"
  verify_schema_contains_selector "$DAST_SCHEMA" ".properties.vulnerabilities.items.properties.assets.items.properties.name"
  verify_schema_contains_selector "$DAST_SCHEMA" ".properties.vulnerabilities.items.properties.assets.items.properties.url"

}
