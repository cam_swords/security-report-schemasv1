# Schemas for GitLab security reports

This repository defines the schemas for the security reports emitted by GitLab security scanners. It defines the reports for:
- [Dependency Scanning](https://docs.gitlab.com/ee/user/application_security/dependency_scanning/)
- [Container Scanning](https://docs.gitlab.com/ee/user/application_security/container_scanning/)
- [Static Application Security Testing](https://docs.gitlab.com/ee/user/application_security/sast/)
- [Dynamic Application Security Testing](https://docs.gitlab.com/ee/user/application_security/dast/)
- [Secret Detection](https://docs.gitlab.com/ee/user/application_security/secret_detection/)
- [Coverage Guided Fuzzing](https://docs.gitlab.com/ee/user/application_security/coverage_fuzzing/)

The schemas are defined using [JSON Schema](https://json-schema.org/). Any security scanner that integrates into GitLab must produce a JSON report that adheres to one of above schemas.

More information about the how and why of the schemas can be found by watching the [Security Report Format Brown Bag Session](https://youtu.be/DqKsdNLXxes).

## How to modify a schema
Schemas are made from a combination of the base `security-report-format.json` schema and an extension schema that belongs to a report type (e.g. `sast-report-format.json`). These files can be found in the `src` directory of the project.

### Determining where the change belongs 
When adding new fields to a schema, please add it to the appropriate place:

- If a field is experimental, it likely belongs in the specific report schema
- If a field is used by few report types, it likely belongs in each report type schema
- If a field is to be used by all report types, it likely belongs in the base schema

### Adding a new field to an extension schema

New fields in extension schemas must be added to the `allOf` array in the source schema file. 
For example, if DAST was to extend the vulnerability definition, and add a new required field called `evidence`:

```json
 "allOf": [
    { "$ref": "security-report-format.json" },
    {
      "properties": {
        "vulnerabilities": {
          "items": {
            "properties": { "evidence": { "type": "string" } },
            "required": [ "evidence" ]
          }
        }
      }
    }
  ]
```

Adding the new field to the normal `properties` field in the extension schema, or redefining `definitions` with the new field definition will not result in the field being contained in the generated schema output.

When adding a new field, please add an appropriate check in the `tests/test-*.sh` file to verify that the merge contains the field you expect.

### Building for extensibility

Keep in mind that as the schemas grow fields will need to be altered to add more content. While the future cannot be predicted, the schemas can be made easy to change with some simple design choices.

For example, the `vendor` property in the following snippet cannot be extended, because it is of type `string`:  
```json
"vendor": { "type": "string" }
```

This can be improved by making `vendor` an `object`.
```json
"vendor": {
    "type": "object",
    "required": ["name"],
    "properties": {
      "name": { "type": "string" }
    }
  }
```

Similarly, each link in the following example cannot be extended: 

```json
"links": {
  "type": "array"
  "items": { "type": "string" }
}
```

This can be improved by making each item in the array an object.

```json
"links": {
  "type": "array",
  "items": {
    "type": "object",
    "required": ["url"],
    "properties": {
      "url": { "type": "string" }
    }
  }
}
```

### Pre-commit expectations

Schemas are released by unifying the base schema with each report schema. All the `$ref` and `allOf` references are inlined, duplicate fields are merged, and the output is written to the `dist` directory.

To ensure that a successful run of the CI Pipeline on the `master` branch of the project can be released,
engineers must do the following:
 
- The CHANGELOG version should be incremented, and a description should be added. Please follow a consistent format.
- The `src/security-report-format.json` `self.version` field should be updated to the same version as the new CHANGELOG entry 
- The `dist` output should be regenerated and checked-in. This can achieved by (or check the docker alternative below):
  - Install `node`, `npm`, and `jq`
  - Run `npm install` from the project directory
  - Run `./scripts/distribute.sh`
- Alternatively, you can use the provided `Dockerfile`:
  - `docker build -t security-report-schemas .`
  - `docker run -it --rm -v $PWD:/security-report-schemas security-report-schemas ./scripts/distribute.sh`

## How to release a new version of the schema

To release a new version of the schema, trigger the manual release job at the end of a successful master CI Pipeline.
This does the following:

- Reads the most recent CHANGELOG entry to determine the version and description.
- Verifies that the version has not already been released.
- Creates a Git tag for the version, pointing to the commit SHA that ran on the master pipeline.
- Creates a GitLab release based on the new Git tag.
- Adds release notes containing CHANGELOG description, and links to download the schemas.
- Creates a schemas-only package and publishes it to the npm public repository with the set SchemaVer value.

Please coordinate and communicate with others when triggering a release.
Releases of the schema do not need to correspond to GitLab releases, in fact, there is no limit as to how often releases can be made. 

## Versioning schemas

This repository follows the [SchemaVer](https://github.com/snowplow/iglu/wiki/SchemaVer) standard to version JSON schemas.

### Classifying changes

For the following situations, you MUST increment the `MODEL.REVISION.ADDITION` version as suggested. You SHOULD refer back to
SchemaVer if your use-case is not listed.

- If you add a new optional field, whether or not it contains required fields, you MUST use an `ADDITION` change.
- If you change an existing field to be required, or add a new required field, you MUST use a `MODEL` change.
- If you remove a field, you MUST use a `REVISION` change.

The use of a `MODEL` change for new required fields will result in many new `MODEL` releases. This is normal when
using SchemaVer, and does not prohibit products that depend on the Secure Report Format from being explicit regarding versions
they support.

### Additional Properties

Secure schemas allow for additional properties to be present in JSON files. This means that the schemas are only concerned with
fields in a Secure Report that are defined by the schema.
The presence of any additional fields will not cause validation to fail.

This is useful for products that produce Secure Reports:

- Experimental fields can be added to a Secure Report, without affecting how the report is used.
- It allows the product to be ahead of the Secure Report Format, when the product team is confident new fields will be merged into the schemas.

Any additional properties added to a Secure Report are considered experimental and may not be supported. For this reason,
adding optional fields to the Secure Report Format is considered an `ADDITION`, not a `REVISION` change.


## Testing

Tests are used to verify that the appropriate fields are present in the Secure schemas.
The tests are implemented using the [`bash_unit`](https://github.com/pgrange/bash_unit) framework.

To run the tests, follow these steps (or use the docker way below):

1. [Install `bash_unit`](https://github.com/pgrange/bash_unit#other-installation) on Mac OS:

    ```bash
    bash <(curl -s https://raw.githubusercontent.com/pgrange/bash_unit/master/install.sh)
    mv ./bash_unit /usr/local/bin
    ```

1. Install [`jq`](https://stedolan.github.io/jq/) on Mac OS:

    ```bash
    brew install jq
    ```

1. Install the required npm modules

    ```bash
    npm install
    ```

1. Run the tests

    ```bash
    ./test/test.sh
    ```

### Testing with Docker

After building the image from the provided `Dockerfile`:

```
docker run -it --rm -v $PWD:/security-report-schemas security-report-schemas ./test/test.sh
```

## Contributing
If you want to help and extend the list of supported scanners, read the
contribution guidelines https://gitlab.com/gitlab-org/security-products/security-report-schemas/-/blob/master/CONTRIBUTING.md
