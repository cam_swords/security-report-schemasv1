#!/usr/bin/env bash

set -e

PROJECT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")/.." &>/dev/null && pwd)"

# shellcheck disable=SC1090 # (dont follow, will be checked separately)
source "$PROJECT_DIR/scripts/error-utils.sh"

# build_release_json_payload builds a payload that can be used to create a GitLab release via the API.
# arguments: [Version] [Changelog description] [Project URL]
# Line lines are stripped from the changelog, otherwise the GitLab release will be malformatted.
build_release_json_payload() {
  local version="$1"
  local changelog_description="$2"
  local project_url="$3"

  if [[ -z "$version" ]]; then
    error "Aborting, version has not been supplied to ${FUNCNAME[0]}"
  fi

  if [[ -z "$changelog_description" ]]; then
    error "Aborting, changelog description has not been supplied to ${FUNCNAME[0]}"
  fi

  if [[ -z "$project_url" ]]; then
    error "Aborting, Project URL has not been supplied to ${FUNCNAME[0]}"
  fi

  local description=""
  description="$description ##### Changes\n"
  description="$description $changelog_description"
  description="$description \n\n"
  description="$description ##### Secure Report Format Schemas\n"
  description="$description - $project_url/-/raw/$version/dist/container-scanning-report-format.json\n"
  description="$description - $project_url/-/raw/$version/dist/dast-report-format.json\n"
  description="$description - $project_url/-/raw/$version/dist/sast-report-format.json\n"
  description="$description - $project_url/-/raw/$version/dist/coverage-fuzzing-report-format.json\n"
  description="$description - $project_url/-/raw/$version/dist/dependency-scanning-report-format.json\n"
  description="$description - $project_url/-/raw/$version/dist/secret-detection-report-format.json\n"

  local unsafe_release_data="{\"tag_name\":\"$version\",\"description\":\"$description\"}"

  # use node to help replace new lines with \n
  local release_data
  local json_type
  release_data=$(echo "$unsafe_release_data" |
    node -e "console.log((require('fs')).readFileSync(process.stdin.fd, 'utf-8').trim().replace(/\n/g, '\\\n'));")
  json_type=$(echo "$release_data" | jq type | sed "s/\"//g")

  if [[ "$json_type" != "object" ]]; then
    error "Aborting, extracted release data type '$json_type' is not a JSON object. Release data: $release_data"
  fi

  local extracted_tag_name
  local extracted_description
  extracted_tag_name=$(echo "$release_data" | jq ".tag_name" | sed "s/\"//g")
  extracted_description=$(echo "$release_data" | jq ".description" | sed "s/\"//g")

  if [[ -z "$extracted_tag_name" ]]; then
    error "Aborting, unable to determine the tag name from the release data $release_data"
  fi

  if [[ -z "$extracted_description" ]]; then
    error "Aborting, unable to determine the description from the release data $release_data"
  fi

  echo "$release_data"
}

# verify_version_not_released ensures that there is not already a release for the version attempting to be released.
# arguments: [GitLab API token] [CI project ID] [Version]
verify_version_not_released() {
  local gitlab_token="$1"
  local project_id="$2"
  local version="$3"

  if [[ -z "$gitlab_token" ]]; then
    error "Aborting, GitLab CI Token has not been supplied to ${FUNCNAME[0]}"
  fi

  if [[ -z "$project_id" ]]; then
    error "Aborting, CI Project ID has not been supplied to ${FUNCNAME[0]}"
  fi

  if [[ -z "$version" ]]; then
    error "Aborting, version has not been supplied to ${FUNCNAME[0]}"
  fi

  local version_url="https://gitlab.com/api/v4/projects/$project_id/repository/tags/$version"

  if curl --silent --fail --show-error --header "private-token:$gitlab_token" "$version_url"; then
    error "Aborting, tag $version already exists. If this is not expected, please remove the tag and try again."
  fi
}

# tag_git_commit uses the GitLab API to create a lightweight Git tag.
# arguments: [GitLab API token] [CI Project ID] [Tag name] [Commit SHA]
tag_git_commit() {
  local gitlab_token="$1"
  local project_id="$2"
  local tag_name="$3"
  local commit_sha="$4"

  if [[ -z "$gitlab_token" ]]; then
    error "Aborting, GitLab CI Token has not been supplied to ${FUNCNAME[0]}"
  fi

  if [[ -z "$project_id" ]]; then
    error "Aborting, CI Project ID has not been supplied to ${FUNCNAME[0]}"
  fi

  if [[ -z "$tag_name" ]]; then
    error "Aborting, tag name has not been supplied to ${FUNCNAME[0]}"
  fi

  if [[ -z "$commit_sha" ]]; then
    error "Aborting, commit SHA has not been supplied to ${FUNCNAME[0]}"
  fi

  local tag_url="https://gitlab.com/api/v4/projects/$project_id/repository/tags?tag_name=$tag_name&ref=$commit_sha"

  curl --silent --fail --show-error --request POST --header "PRIVATE-TOKEN:$gitlab_token" "$tag_url"
}

# create_gitlab_release uses the GitLab API to create a GitLab release.
# The Git tag should be created before this function is run.
# arguments: [GitLab API token] [CI Project ID] [Release payload]
create_gitlab_release() {
  local gitlab_token="$1"
  local project_id="$2"
  local payload="$3"

  if [[ -z "$gitlab_token" ]]; then
    error "Aborting, GitLab CI Token has not been supplied to ${FUNCNAME[0]}"
  fi

  if [[ -z "$project_id" ]]; then
    error "Aborting, CI Project ID has not been supplied to ${FUNCNAME[0]}"
  fi

  if [[ -z "$payload" ]]; then
    error "Aborting, release payload has not been supplied to ${FUNCNAME[0]}"
  fi

  local release_url="https://gitlab.com/api/v4/projects/$project_id/releases"

  curl --silent --fail --show-error --request POST \
    --header "PRIVATE-TOKEN:$gitlab_token" \
    --header 'Content-Type:application/json' \
    --data "$payload" \
    "$release_url"
}

# publish_schemas_to_npm generates a schemas-only package to be published
# for external consumption on the npm package registry.
# arguments: [Version] [Project URL]
publish_schemas_to_npm() {
  local pkg_version="${1#v}"
  local project_url="$2"
  local npm_pkg_scope="@gitlab-org"
  local pkgdir="npmpkg"

  if [[ -z "$pkg_version" ]]; then
    error "Aborting, package version has not been supplied to ${FUNCNAME[0]}"
  fi

  if [[ -z "$project_url" ]]; then
    error "Aborting, CI project URL has not been supplied to ${FUNCNAME[0]}"
  fi

  pushd "$PROJECT_DIR" >/dev/null

  # Build package to publish
  mkdir -p "$pkgdir/dist"
  cp dist/*.json "$pkgdir/dist/"
  cp LICENSE.md "$pkgdir"
  cp CHANGELOG.md "$pkgdir"
  cp src/npmpkg-template/* "$pkgdir"

  # Fill in template files with variable replacements
  if command -v envsubst >/dev/null; then
    envsubst <src/npmpkg-template/package.json >"$pkgdir/package.json"
    envsubst <src/npmpkg-template/README.md >"$pkgdir/README.md"
  else
    eval "cat <<EOF
$(<src/npmpkg-template/package.json)
EOF
" 2>/dev/null >"$pkgdir/package.json"
    eval "cat <<EOF
$(<src/npmpkg-template/README.md)
EOF
" 2>/dev/null >"$pkgdir/README.md"
  fi

  pushd "$pkgdir" >/dev/null
  echo "Built $npm_pkg_scope/security-report-schemas@$pkg_version"
  echo "Publishing to https://registry.npmjs.org"

  npm config set "//gitlab.com/api/v4/projects/$project_id/packages/npm/:_authToken=${CI_JOB_TOKEN}"
  npm publish
  npm logout --scope="$npm_pkg_scope"
  popd >/dev/null

  # cleanup
  rm -r "$pkgdir"
  popd >/dev/null
}
